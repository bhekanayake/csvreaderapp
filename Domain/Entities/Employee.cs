﻿using Domain.Abstract;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.Entities
{
    /// <summary>
    /// Employee entity
    /// </summary>
    public class Employee: CSVBase
    {
        [Required, Key, DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int EmployeeID { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Gender { get; set; } 

        public int QualificationID { get; set; }

        [ForeignKey("QualificationID")]
        public virtual Qualification Qualification { get; set; }

    }
}
