﻿using Domain.Abstract;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.Entities
{
    /// <summary>
    /// Qualification entity
    /// </summary>
    public class Qualification : CSVBase
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int QualificationID { get; set; }

        public string QualificationName { get; set; }
        public string Achievement { get; set; }
        public string YearObtained { get; set; }
    }
}
