namespace Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Employees",
                c => new
                    {
                        EmployeeID = c.Int(nullable: false),
                        FirstName = c.String(),
                        LastName = c.String(),
                        Gender = c.String(),
                        QualificationID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.EmployeeID)
                .ForeignKey("dbo.Qualifications", t => t.QualificationID, cascadeDelete: true)
                .Index(t => t.QualificationID);
            
            CreateTable(
                "dbo.Qualifications",
                c => new
                    {
                        QualificationID = c.Int(nullable: false, identity: true),
                        QualificationName = c.String(),
                        Achievement = c.String(),
                        YearObtained = c.String(),
                    })
                .PrimaryKey(t => t.QualificationID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Employees", "QualificationID", "dbo.Qualifications");
            DropIndex("dbo.Employees", new[] { "QualificationID" });
            DropTable("dbo.Qualifications");
            DropTable("dbo.Employees");
        }
    }
}
