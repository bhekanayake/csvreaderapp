﻿using System;
using System.Collections.Generic;

namespace Domain.Abstract
{
    /// <summary>
    /// CSVBase abstract class which defines the csv reading functionality
    /// </summary>
    public abstract class CSVBase
    {
        public virtual void AssignDataFromCsv(string[] propertyValues, List<string> headers)
        {
            var properties = GetType().GetProperties();

            for (var i = 0; i < properties.Length; i++)
            {
                //If the property value is a subclass of CSVBase, then execute this method for that instance
                if (properties[i].PropertyType.IsSubclassOf(typeof(CSVBase)))
                {
                    var instance = Activator.CreateInstance(properties[i].PropertyType);
                    var instanceProperties = instance.GetType().GetProperties();
                   
                    var m = instance.GetType().GetMethod("AssignDataFromCsv", new Type[] { typeof(string[]) , typeof(List<string>) });
                    m.Invoke(instance, new object[] { propertyValues, headers });
                    properties[i].SetValue(this, instance);

                    i += instanceProperties.Length;
                }
                else
                {
                    var type = properties[i].PropertyType.Name;
                    int index = Helper.GetIndexOfValue(headers, properties[i].Name); 

                    if(index >= 0)
                    { 
                        var value = propertyValues[index];

                        if (value != null)
                        {
                            switch (type)
                            {
                                case "Int32":
                                    properties[i].SetValue(this,
                                                    int.Parse(value));
                                    break;
                                default:
                                    properties[i].SetValue(this, value);
                                    break;
                            }
                        }
                    }
                }
            }
        } 
    }
}
