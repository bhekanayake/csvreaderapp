﻿using Domain.Entities;
using System.Collections.Generic;

namespace Domain.Abstract
{
    /// <summary>
    /// Repository of Employee entities
    /// </summary>
    public  interface IEmployeeRepository
    {
        IEnumerable<Employee> Employees { get; }

        void AddEmployee(Employee employee); 
    }
}
