﻿using Domain.Entities;
using System.Collections.Generic;

namespace Domain.Abstract
{
    /// <summary>
    /// Repository of Qualification entities
    /// </summary>
    public interface IQualificationRepository
    {
        IEnumerable<Qualification> Qualifications { get; }

        void AddQualification(Qualification qualification); 
    }
}
