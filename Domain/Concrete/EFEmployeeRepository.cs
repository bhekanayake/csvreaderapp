﻿using Domain.Abstract;
using Domain.Entities;
using System.Collections.Generic;

namespace Domain.Concrete
{
    /// <summary>
    /// Employee repository implementation (EF)
    /// </summary>
    public class EFEmployeeRepository : IEmployeeRepository
    {
        private readonly EFDBContext context = new EFDBContext();

        public IEnumerable<Employee> Employees
        {
            get { return context.Employees; }
        } 

        public void AddEmployee(Employee employee)
        {
            Employee dbEntry = context.Employees.Find(employee.EmployeeID);

            if (dbEntry == null)
            { 
                context.Employees.Add(employee);

                context.SaveChanges();
            }
        } 
    }
}
