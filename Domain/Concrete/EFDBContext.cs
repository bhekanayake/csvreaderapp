﻿using Domain.Entities;
using System.Data.Entity;

namespace Domain.Concrete
{
    /// <summary>
    /// Entity framework context class 
    /// </summary>
    public class EFDBContext : DbContext
    {
        public EFDBContext()
            : base("EFDBContext")
        {
        }

        public DbSet<Employee> Employees { get; set; }
        public DbSet<Qualification> Qualifications { get; set; }
    }
}
