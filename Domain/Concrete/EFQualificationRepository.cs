﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Entities;
using Domain.Abstract;

namespace Domain.Concrete
{
    /// <summary>
    /// Qualification repository implementation (EF)
    /// </summary>
    public class EFQualificationRepository : IQualificationRepository
    {
        private readonly EFDBContext context = new EFDBContext();

        public IEnumerable<Qualification> Qualifications
        {
            get { return context.Qualifications; }
        } 

        public void AddQualification(Qualification qualification)
        {
            Qualification dbEntry = context.Qualifications.Find(qualification.QualificationID);

            if (dbEntry == null)
            {
                context.Qualifications.Add(qualification); 
                context.SaveChanges();
            }
        } 
    }
}
