﻿using Domain.Abstract;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Domain.Concrete
{
    /// <summary>
    /// Generic Csv reader implementation
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class CsvReader<T> where T : CSVBase, new()
    { 
        public IEnumerable<T> Read(string filePath)
        {
            var objects = new List<T>();

            using (var sr = new StreamReader(filePath))
            {
                bool headersRead = false;
                string line;
                List<string> headers = new List<string>();

                //Iterate through all csv lines and populate entity instances
                do
                {
                    line = sr.ReadLine();

                    if (line != null && headersRead)
                    {
                        var obj = new T();
                        var propertyValues = line.Split(',');
                        obj.AssignDataFromCsv(propertyValues, headers);
                        objects.Add(obj);

                    }
                    if (!headersRead && line !=null)
                    {
                        headers = line.Split(',').ToList<string>();
                        headersRead = true;
                    }
                } while (line != null);
            }

            return objects;
        }
    }
}
