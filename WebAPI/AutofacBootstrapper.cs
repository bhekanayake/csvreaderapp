﻿using System.Web.Http;

namespace WebAPI
{
    /// <summary>
    /// Autofac bootstrapper
    /// </summary>
    public class AutofacBootstrapper
    { 
        public static void Run()
        { 
            AutofacConfig.Initialize(GlobalConfiguration.Configuration);
        }  
    }
}