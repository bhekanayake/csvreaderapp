﻿using Domain.Abstract;
using Domain.Concrete;
using Domain.Entities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http;

namespace WebAPI.Controllers
{
    /// <summary>
    /// CSVReader controller class
    /// </summary>
    public class CSVReaderController : ApiController
    {
        private readonly IEmployeeRepository _repository;

        /// <summary>
        /// Constructor 
        /// </summary>
        /// <param name="repository">This parameter is passed at runtime through Autofac IOC</param>
        public CSVReaderController(IEmployeeRepository repository)
        {
            _repository = repository;
        } 
         
        /// <summary>
        /// Uploading a file
        /// </summary>
        /// <returns></returns>
        [HttpPost] 
        public IHttpActionResult UploadFile()
        {
            try
            {
                if (HttpContext.Current.Request.Files.AllKeys.Any())
                {
                    if (HttpContext.Current.Request.Files.Count == 1)
                    {
                        var httpPostedFile = HttpContext.Current.Request.Files[0];

                        if (httpPostedFile != null)
                        {
                            if (Path.GetExtension(httpPostedFile.FileName).ToLower() != ".csv")
                            {
                                return Content(HttpStatusCode.BadRequest, "Wrong file type sent.");
                            }

                            var fileSavePath = System.IO.Path.Combine(HttpContext.Current.Server.MapPath("~/csv_files"), httpPostedFile.FileName);

                            httpPostedFile.SaveAs(fileSavePath);

                            List<Employee> employees = new CsvReader<Employee>().Read(fileSavePath).ToList<Employee>();

                            foreach (Employee employee in employees)
                            {
                                _repository.AddEmployee(employee);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.InternalServerError, "The file could not be processed due to an error.");
            }

            return Content(HttpStatusCode.Created, "File read successfully");
        }
    }
}
