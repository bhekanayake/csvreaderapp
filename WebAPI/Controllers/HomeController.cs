﻿using System.Web.Http;

namespace WebAPI.Controllers
{
    [RoutePrefix("")]
    public class HomeController : ApiController
    {
        [Route("")] 
        public string Get()
        {
            return "Waiting for connections...";
        } 
    }
}
