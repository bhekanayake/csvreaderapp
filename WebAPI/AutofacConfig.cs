﻿using Autofac;
using Autofac.Integration.WebApi;
using Domain.Abstract;
using Domain.Concrete;
using System.Reflection;
using System.Web.Http;

namespace WebAPI
{
    /// <summary>
    /// Autofac config file
    /// </summary>
    public class AutofacConfig
    { 
        public static IContainer Container;

        public static void Initialize(HttpConfiguration config)
        {
            Initialize(config, RegisterServices(new ContainerBuilder()));
        } 

        public static void Initialize(HttpConfiguration config, IContainer container)
        {
            config.DependencyResolver = new AutofacWebApiDependencyResolver(container);
        }

        /// <summary>
        /// Map concrete objects with interfaces
        /// </summary>
        /// <param name="builder"></param>
        /// <returns></returns>
        private static IContainer RegisterServices(ContainerBuilder builder)
        { 
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());

            builder.RegisterType<EFEmployeeRepository>()
                    .As<IEmployeeRepository>()
                    .InstancePerRequest();

            builder.RegisterType<EFQualificationRepository>()
                    .As<IQualificationRepository>()
                    .InstancePerRequest(); 
 
            Container = builder.Build();

            return Container;
        }

    }
    
}